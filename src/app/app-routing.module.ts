import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./dashboard/home/home.component";
import { AddUserComponent } from "./dashboard/add-user/add-user.component";
import { UserListComponent } from "./dashboard/user-list/user-list.component";
import { AddNoteComponent } from "./dashboard/add-note/add-note.component";
import { ShowNoteComponent } from "./dashboard/show-note/show-note.component";
import { AddCategoryComponent } from "./dashboard/add-category/add-category.component";
import { ShowCategoryComponent } from "./dashboard/show-category/show-category.component";
import { EditNoteComponent } from "./dashboard/edit-note/edit-note.component";

const routes: Routes = [
  { path: "", component: LoginComponent },
  { path: "dashboard", component: HomeComponent },
  { path: "dashboard/users/add", component: AddUserComponent },
  { path: "dashboard/users", component: UserListComponent },
  { path: "dashboard/categories", component: ShowCategoryComponent },
  { path: "dashboard/categories/add", component: AddCategoryComponent },
  { path: "dashboard/notes/add", component: AddNoteComponent },
  { path: "dashboard/notes", component: ShowNoteComponent },
  { path: "dashboard/notes/:id", component: EditNoteComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
