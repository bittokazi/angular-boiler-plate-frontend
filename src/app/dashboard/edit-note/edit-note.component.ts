import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ApiServiceService } from "./../api-service.service";

@Component({
  selector: "app-edit-note",
  templateUrl: "./edit-note.component.html",
  styleUrls: ["./edit-note.component.css"]
})
export class EditNoteComponent implements OnInit {
  public form;
  public categories;
  public noteId;

  constructor(
    public apiService: ApiServiceService,
    public router: Router,
    public route: ActivatedRoute
  ) {
    this.noteId = route.snapshot.params["id"];
    this.form = new FormGroup({
      id: new FormControl(this.noteId),
      title: new FormControl(""),
      description: new FormControl(""),
      category: new FormGroup({
        id: new FormControl(0)
      })
    });
  }

  ngOnInit(): void {}

  submit() {
    if (this.form.value.category.id != 0) {
      this.apiService.updateNote(
        this.form.value,
        response => {
          this.router.navigate(["/dashboard/notes"]);
        },
        error => {}
      );
    }
  }

  authSuccess(event) {
    this.apiService.categoryList(
      response => {
        this.categories = response.body;
      },
      error => {}
    );
    this.apiService.getNote(
      this.noteId,
      response => {
        this.form = new FormGroup({
          id: new FormControl(response.body.id),
          title: new FormControl(response.body.title),
          description: new FormControl(response.body.description),
          category: new FormGroup({
            id: new FormControl(response.body.category.id)
          })
        });
      },
      error => {}
    );
  }
}
