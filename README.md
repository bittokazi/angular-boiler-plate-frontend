# AngularBoilerPlateApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Also you can change `baseUrl: "http://localhost:8080/sb-boiler-plate-backend"` in `environment.ts` file if you are running backend somewhere else.
