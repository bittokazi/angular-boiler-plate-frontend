import { Component, OnInit } from "@angular/core";
import { ApiServiceService } from "./../api-service.service";

@Component({
  selector: "app-show-note",
  templateUrl: "./show-note.component.html",
  styleUrls: ["./show-note.component.css"]
})
export class ShowNoteComponent implements OnInit {
  public currentCat = 0;
  public categories = [];
  public notes = [];
  public allNotes = [];

  constructor(public apiService: ApiServiceService) {}

  ngOnInit(): void {}

  selectCat(id) {
    this.currentCat = id;
    this.notes = this.filterNotes();
  }

  filterNotes() {
    return this.allNotes.filter(note => {
      if (this.currentCat == 0) return true;
      else if (note.category.id == this.currentCat) return true;
      return false;
    });
  }

  isActive(id) {
    return id == this.currentCat ? true : false;
  }

  authSuccess(event) {
    this.apiService.categoryList(
      response => {
        this.categories = response.body;
      },
      error => {}
    );
    this.loadNotes();
  }

  loadNotes() {
    this.apiService.noteList(
      response => {
        this.allNotes = response.body;
        this.notes = this.filterNotes();
      },
      error => {}
    );
  }

  deleteNote(id) {
    this.apiService.deleteNote(
      id,
      response => {
        this.loadNotes();
      },
      error => {}
    );
  }
}
