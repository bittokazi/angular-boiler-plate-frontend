import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "./../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class ApiServiceService {
  constructor(private http: HttpClient) {}

  addUser(data, success, error) {
    data.firstName = "n/a";
    data.lastName = "n/a";
    data.enabled = true;
    data.changePassword = true;
    this.http
      .post(environment.baseUrl + "/api/users", data, { observe: "response" })
      .subscribe({
        next: res => {
          success(res);
        },
        error: err => {
          error(err);
        }
      });
  }

  userList(success, error) {
    this.http
      .get(environment.baseUrl + "/api/users", { observe: "response" })
      .subscribe({
        next: res => {
          success(res);
        },
        error: err => {
          error(err);
        }
      });
  }

  addCategory(data, success, error) {
    this.http
      .post(environment.baseUrl + "/api/categories", data, {
        observe: "response"
      })
      .subscribe({
        next: res => {
          success(res);
        },
        error: err => {
          error(err);
        }
      });
  }

  categoryList(success, error) {
    this.http
      .get(environment.baseUrl + "/api/categories", { observe: "response" })
      .subscribe({
        next: res => {
          success(res);
        },
        error: err => {
          error(err);
        }
      });
  }

  addNote(data, success, error) {
    this.http
      .post(environment.baseUrl + "/api/notes", data, {
        observe: "response"
      })
      .subscribe({
        next: res => {
          success(res);
        },
        error: err => {
          error(err);
        }
      });
  }

  updateNote(data, success, error) {
    this.http
      .put(environment.baseUrl + "/api/notes/" + data.id, data, {
        observe: "response"
      })
      .subscribe({
        next: res => {
          success(res);
        },
        error: err => {
          error(err);
        }
      });
  }

  noteList(success, error) {
    this.http
      .get(environment.baseUrl + "/api/notes", { observe: "response" })
      .subscribe({
        next: res => {
          success(res);
        },
        error: err => {
          error(err);
        }
      });
  }

  getNote(id, success, error) {
    this.http
      .get(environment.baseUrl + "/api/notes/" + id, { observe: "response" })
      .subscribe({
        next: res => {
          success(res);
        },
        error: err => {
          error(err);
        }
      });
  }

  deleteNote(id, success, error) {
    this.http
      .delete(environment.baseUrl + "/api/notes/" + id, { observe: "response" })
      .subscribe({
        next: res => {
          success(res);
        },
        error: err => {
          error(err);
        }
      });
  }
}
