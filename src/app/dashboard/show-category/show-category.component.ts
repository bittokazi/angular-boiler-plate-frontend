import { Component, OnInit } from "@angular/core";
import { ApiServiceService } from "./../api-service.service";

@Component({
  selector: "app-show-category",
  templateUrl: "./show-category.component.html",
  styleUrls: ["./show-category.component.css"]
})
export class ShowCategoryComponent implements OnInit {
  public categories;

  constructor(public apiService: ApiServiceService) {}

  ngOnInit(): void {}

  authSuccess(event) {
    this.apiService.categoryList(
      response => {
        this.categories = response.body;
      },
      error => {}
    );
  }
}
