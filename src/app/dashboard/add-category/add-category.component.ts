import { Component, OnInit } from "@angular/core";
import { ApiServiceService } from "./../api-service.service";
import { FormGroup, FormControl } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-add-category",
  templateUrl: "./add-category.component.html",
  styleUrls: ["./add-category.component.css"]
})
export class AddCategoryComponent implements OnInit {
  public form;

  constructor(public apiService: ApiServiceService, public router: Router) {
    this.form = new FormGroup({
      title: new FormControl("")
    });
  }

  ngOnInit(): void {}

  authSuccess(event) {
    console.log("authSuccess", event);
  }

  submit() {
    this.apiService.addCategory(
      this.form.value,
      response => {
        this.router.navigate(["/dashboard/categories"]);
      },
      error => {}
    );
  }
}
