import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ApiServiceService } from "./../api-service.service";

@Component({
  selector: "app-add-note",
  templateUrl: "./add-note.component.html",
  styleUrls: ["./add-note.component.css"]
})
export class AddNoteComponent implements OnInit {
  public form;
  public categories;

  constructor(public apiService: ApiServiceService, public router: Router) {
    this.form = new FormGroup({
      title: new FormControl(""),
      description: new FormControl(""),
      category: new FormGroup({
        id: new FormControl(0)
      })
    });
  }

  ngOnInit(): void {}

  submit() {
    if (this.form.value.category.id != 0) {
      this.apiService.addNote(
        this.form.value,
        response => {
          this.router.navigate(["/dashboard/notes"]);
        },
        error => {}
      );
    }
  }

  authSuccess(event) {
    this.apiService.categoryList(
      response => {
        this.categories = response.body;
      },
      error => {}
    );
  }
}
